Installation notes
------------------

 1. Download the ODataProducer for PHP from
    "https://github.com/MSOpenTech/odataphpprod".
 2. Place the ODataProducer (library/ODataProducer) in sites/all/libraries
 3. Enable the ODataProducer module.
